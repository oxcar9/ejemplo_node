FROM node:14-alpine 
WORKDIR /usr/local/app-example
COPY app.js /usr/local/app-example
ADD *.json /usr/local/app-example
ENV USER=pepitoquechua
RUN npm install
EXPOSE 5000
CMD npm run start